__author__ = ['Mark Anthony R. Pequeras']
__language__ = ['Python']
__version__ = '1.0b'
__license__ = ['MIT']

"""
Sample iTexMoAPI Usage
"""

# Import iTexMoAPI (itexmo.py)
from itexmo import Imapi

MyNumber = "09289887311"
MyMessage = "Hello There!?"
MyApi = "TEST"



# Set Number,Message and API Parameters to initialize via Imapi class.
API = Imapi(Number=MyNumber,Message=MyMessage,Api=MyApi,Debug=1)

# Now Call Set() to Setup your Informations
API.Set(API)

# Ready to Send! call the Send Function!
API.Send()





