Usage:

> - Import the Api, File: itexmo.py

```
from itexmo import Imapi
```

-------


> - Set All Information using Imapi Class 

```
API = Imapi( "09289887311" , "THI IS A MESSAGE" ,"MYAPI" )
```

--------

> - Save All information into Set() Function via API Variable

```
API.Set(API)
```
--------

> - Let's Send using the Send() Function!

```
API.Send()
```

--------
--------

**Full Usage Example**:

```
from itexmo import Imapi

API = Imapi(Number=MyNumber,Message=MyMessage,Api=MyApi,Debug=1)

API.Set(API)

API.Send()
```

