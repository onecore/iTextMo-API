# --*-- coding-utf8 --*--

__author__ = ['Mark Anthony R. Pequeras']
__version__ = 0.1
__license__ = ['MIT']
__website__ = 'http://www.marp.me/'
__info__ = """
This Python Module is a Port of iTexMo-API in C Language into Python (Enhanced Source)
"""

class ItexMoApiError(Exception):
    """
    Custom API Exception
    """
    pass

class Imapi:
    """
    ItexMoApi Class (Parameters initializes here)
    """

    def __init__(self,Number=None,Message=None,Api=None,Debug=None):
        """
        Initialize All
        """
        self.Number = Number
        self.Message = Message
        self.Api = Api
        self.Parsed = None
        self.Debug = Debug
        #self.Type = Type #lambda: None if Type is None else (str,int,long,dict)

    def Recieve(self,Data):
        """
        Reciever Function
        """
        pass


    def Set(self,Function,*args):
        """
        Function helper for Getting function attributes.
        """
        FunctionAttr = [Function.Number,Function.Message,Function.Api]
        _Diction = {"1":Function.Number,"2":Function.Message,"3":Function.Api}

        self.Parsed = _Diction
        try:
            for Attr in FunctionAttr:
                if Attr == None:
                    raise
                    break
                else:
                    break
        except:
            if self.Debug:
                ItexMoApiError("Please Call Imapi(Number='YOURNUMBER',Message='YOUR MESSAGE',Api='YOUAPI') class!")

    def Send(self):
        """
        Accepts only Dictionary type for Sending POST
        requests through requests library
        """
        import requests
        Headers = {'content-type': 'application/x-www-form-urlencoded'}
        ApiUrl = "http://www.itexmo.com/php_api/api.php"
        try:
            requests.post(ApiUrl, params=self.Parsed, headers=Headers)
            if self.Debug:
                print "Processed!"
        except requests.ConnectionError:
            if self.Debug:
                raise ItexMoApiError("Internet Connection Required!")

